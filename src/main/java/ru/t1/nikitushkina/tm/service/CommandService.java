package ru.t1.nikitushkina.tm.service;

import ru.t1.nikitushkina.tm.api.repository.ICommandRepository;
import ru.t1.nikitushkina.tm.api.service.ICommandService;
import ru.t1.nikitushkina.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
