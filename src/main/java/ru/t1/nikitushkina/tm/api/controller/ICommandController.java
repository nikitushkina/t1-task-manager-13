package ru.t1.nikitushkina.tm.api.controller;

public interface ICommandController {

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showSystemInfo();

    void showVersion();

    void showHelp();

}
