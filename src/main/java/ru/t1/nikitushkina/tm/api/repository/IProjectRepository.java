package ru.t1.nikitushkina.tm.api.repository;

import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void clear();

    Project add(Project project);

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    boolean existsById(String id);

}
